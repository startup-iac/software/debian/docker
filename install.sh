#!/bin/bash

# Docker params
echo "vm.max_map_count=524288" | sudo tee -a /etc/sysctl.conf
echo "fs.file-max=131072" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
echo "* soft nofile 131072" | sudo tee -a /etc/security/limits.conf
echo "* hard nofile 131072" | sudo tee -a /etc/security/limits.conf
echo "* soft nproc 8192" | sudo tee -a /etc/security/limits.conf
echo "* hard nproc 8192" | sudo tee -a /etc/security/limits.conf

# Add Docker Repository
wget -qO - https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o $KEYRINGS_PATH/docker-keyring.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/docker-keyring.gpg] \
    https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
    | sudo tee /etc/apt/sources.list.d/docker.list

# Update Repositories
sudo apt-get update

# Install Docker
sudo apt-get install -y docker-ce docker-ce-cli containerd.io 
sudo usermod -aG docker vagrant # No sudo for docker command
